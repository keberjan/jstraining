<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.w3.org/1999/xhtml
                          http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd">

	<head>
		<title>JavaScript Training</title>
		<script type="text/javascript" src="jquery.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	<body>
        <?php
        $id = $_GET["id"];
        if($id == "") {
            include "pages/index.php";
        } else {
            include "pages/$id.php";
        }
        ?>
		<script type="text/javascript" src="training.js"></script>
	</body>
</html>
