$(document).ready(function() {

	$("#username_input").focus(function() {
		if(this.value == "username") {
			this.value = "";
		}
	});

	$("#username_input").blur(function() {
		if(this.value == "") {
			this.value = "username";
		}
	});

	$("#email_input").focus(function() {
		if(this.value == "example@example.com") {
			this.value = ""
		}
	});

	$("#email_input").blur(function() {
		if(this.value == "") {
			this.value = "example@example.com";
		}
	});

	$("h1").click(function() {
		$("#parag").slideToggle();
	});

	$("#password_input").focus(function() {
		if(this.value == "123456") {
			this.value = "";
		}
	});

	$("#password_input").blur(function() {
		if(this.value == "") {
			this.value = "123456";
		}
	});

	$("#password2_input").focus(function() {
		if(this.value == "123456") {
			this.value = "";
		}
	});

	$("#password2_input").blur(function() {
		if(this.value == "") {
			this.value = "123456";
		}
	});

    $("#l_username_input").focus(function() {
        if(this.value == "username") {
            this.value = "";
        }
    });

    $("#l_username_input").blur(function() {
        if(this.value == "") {
            this.value = "username";
        }
    });

    $("#l_password_input").focus(function() {
        if(this.value == "123456") {
            this.value = "";
        }
    });

    $("#l_password_input").blur(function() {
        if(this.value == "") {
            this.value = "123456";
        }
    });

	$("#register_button").click(function() {
		var username = $("#username_input").val();
		var email = $("#email_input").val();
		var password = $("#password_input").val();
		var password2 = $("#password2_input").val();

		if(username != "" && email != "" && password != "" && password2 != "") {
			if(password == password2) {
                var dataString = "username=" + username + "&email=" + email + "&password=" + password + "&password2=" + password2;
                $.ajax({
                  type: "GET",
                  url: "register_b.php",
                  datatype: "html",
                  data: dataString,
                  success: function(data) {
                      if(data == 1) {
                          $("#response_fail").html("User already exists");
                          $("#response_fail").fadeIn(500);
                      }

                      if(data == 2) {
                          $("#response_fail").html("Email already exists");
                          $("#response_fail").fadeIn(500);
                      }

                      if(data == 3) {
                          $("#response_fail").css("display", "none");
                          $("#response_success").html("Uporabnik registriran");
                          $("#response_success").fadeIn(500);
                      }
                }
                });
            } else {
                alert("Gesli se ne ujemata");
            }
		} else {
			alert("polja so prazna");
		}
	});

    $("#login_button").click(function() {
        var username = $("#l_username_input").val();
        var password = $("#l_password_input").val();

        console.info(username);
        console.info(password);

        if(username != "" && password != "") {
            var dataString = "username=" + username + "&password=" + password;
            $.ajax({
                type: "GET",
                url: "login_b.php",
                datatype: "html",
                data: dataString,
                success: function(data) {
                    if(data == 1) {
                        $("#login_response_fail").html("Error. Contact system administrator");
                        $("#login_response_fail").fadeIn(500);
                    }

                    if(data == 2) {
                        $("#login_response_fail").html("Wrong username or password");
                        $("#login_response_fail").fadeIn(500);
                    }

                    if(data == 3) {
                        $("#login_response_fail").html("Wrong username or password");
                        $("#login_response_fail").fadeIn(500);
                    }

                    if(data == 4) {
                        $("#login_response_fail").css("display", "none");
                        $("#login_response_success").html("Logged in... Refreshing...");
                        $("#login_response_success").fadeIn(500);
                        window.location.href = 'index.php';
                    }
                }
            });
        } else {
            alert("Fill all fields");
        }
    });
});
