<?php
session_start();
ob_start();

$username = $_GET["username"];
$password = $_GET["password"];


if(!empty($username) and !empty($password)) {

    $sql_server = "localhost";
    $sql_database = "jstraining";
    $sql_username = "root";
    $sql_password = "C1sc0N3tw0rk1ng";

    try {
        $sql_connection = new PDO("mysql:host={$sql_server};dbname={$sql_database}",$sql_username,$sql_password);
        $sql_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $sql_connection->prepare("SELECT * FROM users WHERE username = :username");
        $stmt->execute(array(':username'=>$username));
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

        if($userRow == true) {
            if(password_verify($password, $userRow["password"])) {
                $_SESSION["logged_in"] = "true";
                echo "4";
            } else {
                echo "3";
            }
        } else {
            echo "2";
        }

    } catch (PDOException $e) {
        echo "1";
    }
} else {
    echo "1";
}
?>
