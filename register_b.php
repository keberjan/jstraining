<?php
session_start();
ob_start();

$username = $_GET["username"];
$email = $_GET["email"];
$password = $_GET["password"];
$password2 = $_GET["password2"];

if(!empty($username) && !empty($email) && !empty($password) && !empty($password2)) {
    if($password == $password2) {
        $sql_server = "localhost";
        $sql_database = "jstraining";
        $sql_username = "root";
        $sql_password = "C1sc0N3tw0rk1ng";

        $encrypted_password = password_hash($password, PASSWORD_DEFAULT);

        try {
            $sql_connection = new PDO("mysql:host={$sql_server};dbname={$sql_database}",$sql_username,$sql_password);
            $sql_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $sql_connection->prepare("SELECT COUNT(username) AS num FROM users WHERE username = :username");
            $stmt->bindValue(":username", $username);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            if($row['num'] > 0) {
                echo "1";
                die();
            }

            $stmt = $sql_connection->prepare("SELECT COUNT(email) AS num FROM users WHERE email = :email");
            $stmt->bindValue(":email", $email);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            if($row['num'] > 0) {
                echo "2";
                die();
            }

            $stmt = $sql_connection->prepare("INSERT INTO users(username, email, password) VALUES (:uname, :uemail, :upassword)");

            $stmt->bindparam(":uname", $username);
            $stmt->bindparam(":uemail", $email);
            $stmt->bindparam(":upassword", $encrypted_password);
            $stmt->execute();

            echo "3";
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    } else {
       echo "Geslo se ne ujemata";
    }
} else {
    echo "Izpolni vse";
}
?>
